#include <stdio.h>	//unsigned long fopen
#include <stdlib.h>
#include <string.h>
#include "libscrypt.h"


#define  NUM 4096
int  main(int  argc, char **argv)
{
  char buffa[NUM];
  char buffb[NUM];

  //set all elements to 0
  memset(buffa, 0, NUM);
  //set all elements to 1
  memset(buffb, 1, NUM); 

  //set specific 'bits' (elements) 
  buffa[NUM-1] = 1;
  buffa[2] = 1;
  buffa[2] = 1 ;

  //set up numbers into structure 
  mpn_z a = { buffa, NULL, NUM, 0};
  mpn_z b = { buffb, NULL, NUM, 0};
  mpn_z c = { NULL, NULL, 0, 0};
  
  //double dabble algorithm turns binary rep to decimal rep
  double_dabble(&a);
  double_dabble(&b);
  mpn_multiply(&a,&b,&c);
  double_dabble(&c);


  mpn_print_base10(&a);
  mpn_print_base10(&b);
  printf("multiplying\n");
  mpn_print_base10(&c);

 // remove_zeros(&c, c.length);
  //print size or length of the numbers in each representation
 printf("A bin %ld dec %ld\n", a.length, a.base10_length);
 printf("B bin %ld dec %ld\n", b.length, b.base10_length);
 printf("C bin %ld dec %ld\n", c.length, c.base10_length);


  //estimating the size a base10 number will be from the binary size
  double conv = (double)c.base10_length * log2(10);
  int bit_number = (int)conv + 1;

  printf("c's number of bin digits are %d\nname changed?\n",bit_number);

  reverse_double_dabble(&c);
  }


























