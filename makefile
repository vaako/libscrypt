PROGRAM=app

${PROGRAM} : main.o  libscrypt.o
	gcc -g -lm main.o  libscrypt.o  -o ${PROGRAM}

main.o : main.c
	gcc -g -c main.c

clean : *.o
	rm -f *.o
	rm -f ${PROGRAM}

libscrypt.o : libscrypt.c
	gcc -g -c libscrypt.c

