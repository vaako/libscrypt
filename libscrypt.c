//vaako software development 

#include "libscrypt.h"
#define REGSZE 4

char *double_dabble(mpn_z *n)
{

// need a buffer length n +
  char *scratch;
	char *last_register;
	char *first_register;
	unsigned long i,j;
	unsigned long bin_num_length = n->length;
	unsigned long ceiling = REGSZE+(BCD* (int)ceil(n->length/3));		
	unsigned long scratch_length  = bin_num_length  + ceiling;
	unsigned long decimal_number;
	unsigned long total_registers=1;
	

	scratch = (char *)calloc(scratch_length, sizeof(char));
	first_register = (scratch+(ceiling-REGSZE));;
	last_register  = first_register;
	

	//load number BACKWARDS starting from back of buffer
	for(i=scratch_length-1,j=0;j<bin_num_length;j++,i--)
	{
	  scratch[i] = n->binary_str[j];
	}

	for(unsigned long bit=0; bit < bin_num_length;bit++)
	{
    shift_left(scratch, scratch_length);		
		if(bit==bin_num_length-1)
		{
			break;
		}
		char *reg=first_register;
		for(unsigned long  registers=0; registers < total_registers;registers++)
		{	
		  decimal_number = reg_to_number(reg);
			if(decimal_number >= 5)//if gt 5 chamge vslue
			{
				test_reg(reg);
			}
			if(last_register[0] == 1) //new reg begins
			{
			  last_register = last_register-REGSZE;
				total_registers++;
			}
				reg -= REGSZE;
		}
	}
	
  char *baseten = (char *)malloc(sizeof(char) * total_registers);

	for(unsigned long i = 0;i < total_registers;i++)
	{
		baseten[i] = reg_to_number(last_register+(i*REGSZE));
	} 

	n->base10_str = baseten;
	n->base10_length = total_registers;



}//end double_dabble algo

void reverse_double_dabble(mpn_z *n)
{
 //each digit is 4 bits
 int  registers = n->base10_length * REGSZE;
 double binary_bit_num  = (double)n->base10_length  * log2(10);
 int  bits = (int)binary_bit_num + 1;
 unsigned long scratch_length = registers + bits;
 char *scratch = (char *)malloc(sizeof(char) * scratch_length);

 char *first_bin_digit; 

  printf("dec len %ld bin est %d\n", n->base10_length, bits);

  free(scratch);
}




void 	mpn_print_base10(mpn_z *n)
{

	printf("base10 number::");
	for(int i=0; i < n->base10_length;i++)
	{
		printf("%d", n->base10_str[i]); 
	}
	printf("\n");
}

void mpn_print_mstols(char *n, unsigned long length)
{
	for(unsigned long i=length-1;i > 0;i--)
	  printf("%d", n[i]);

	printf("\n");

}

//Must be able to work out size ?
void mpn_subtract(mpn_z *a, mpn_z *b, mpn_z *c)
{



}



static void shift_left(char *scratch, unsigned long scratch_length)
{
	unsigned long max_expo = scratch_length-1;
	size_t cur_expo;
	char *current_bit = scratch;
	//k backwards down buffer shifting bits to left
	for(unsigned long i = 0;i < max_expo; i++,current_bit++)
	{	
		*(current_bit) = *(current_bit+1);
	}
	scratch[max_expo] = 0;;
}

//test reg > 4 and if so add 3
static void test_reg(char *r)
{
	unsigned long number = reg_to_number(r);
	
	switch(number) {
		case 5:	
			r[0] =1;
			r[1] =0;
			r[2] =0;
			r[3] =0;
			break;
		case 6:	
			r[0] =1;
			r[1] =0;
			r[2] =0;
			r[3] =1;
			break;
		case 7:
			r[0] =1;
			r[1] =0;
			r[2] =1;
			r[3] =0;
			break;
		case 8:	
			r[0] =1;
			r[1] =0;
			r[2] =1;
			r[3] =1;
			break;
		case 9:	
			r[0] =1;
			r[1] =1;
			r[2] =0;
			r[3] =0;
			break;
		default :
			break;
	}

}


static unsigned long reg_to_number(char *p)
{
	unsigned long number = 0;

	for(unsigned long n = 0; n < 4;n++)
	{
		switch(n) {
			case 0:
				if (*p == 1)
					number += 8;
					break;
			case 1:
				if (*p==1)
					number+=4;
					break;
			case 2:
				if (*p==1)
					number+=2;
					break;
			case 3:
				if (*p==1)
					number+=1;
			 break;
			 default :
			 	break;
		}
		p++;
	
		}
	return number;
}


// a * b = c;       
void mpn_add(mpn_z *a, mpn_z *b, mpn_z *c)
{
	unsigned long larger;
	unsigned long smaller;
	unsigned long new_length;
	unsigned long carry = 0;
	unsigned long tmp_add = 0;
	unsigned long bit;
	char *s;
	char *l;


	if((a->length == b->length) || (a->length > b->length))
	{
		larger = a->length;
		smaller = b->length;
		l = a->binary_str;
		s = b->binary_str;
	}
	else
	{
		larger 	= b->length;
		smaller = a->length;
		l = b->binary_str;
		s = a->binary_str;
	}

	if(c->binary_str != NULL )
	{
		free(c->binary_str);
	}

	new_length = larger + 1;
	char *new_string= (char *)calloc(new_length, 1);
	c->binary_str = new_string;
	c->length = new_length;

	//copy largest string unsigned longo c first
	for(unsigned long i=0; i < larger;i++)
	{
		c->binary_str[i] = l[i];
	}

	for( bit=0;bit <  smaller; bit++)
	{
		tmp_add = c->binary_str[bit] + s[bit] + carry;
		switch (tmp_add) {
			case 0:
				c->binary_str[bit] = 0;
				break;
			case 1:
				c->binary_str[bit] = 1;
				carry = 0;
				break;
			case 2:
				c->binary_str[bit] = 0;
				carry = 1;
				break;
			case 3:
				c->binary_str[bit] = 1;
				carry = 1;
				break;
			default:
				break;
		}
	}
	
	if(carry == 1)
	{
		tmp_add = c->binary_str[bit] + 1;
		if(tmp_add == 1)
			c->binary_str[bit] = 1;

		if(tmp_add == 2)
			c->binary_str[bit] = 1;
	}


}




void mpn_free(mpn_z *a)
{
	if(a->binary_str != NULL)
		free(a->binary_str);

	if(a->base10_str != NULL)
		free(a->base10_str);

}

static void shift_right(char *binary_scratch, unsigned long length)
{
	unsigned long last_digit = length-1;
	unsigned long current_bit;
	char *bits = binary_scratch;

	for(current_bit=last_digit;current_bit > 0;current_bit--)
	{
		binary_scratch[current_bit] = binary_scratch[current_bit-1];
	}
	binary_scratch[current_bit] = 0;

}


void mpn_multiply(mpn_z *a, mpn_z *b, mpn_z *c)
{
	unsigned long total  = a->length + b->length;
  unsigned long smaller;
	unsigned long larger;
	char *l,*s;
	char *ctmp = (char *)malloc(sizeof(char) * total);
	memset(ctmp, 0 , total);



if((a->length == b->length) || (a->length > b->length))
	{
		larger = a->length;
		smaller = b->length;
		l = a->binary_str;
		s = b->binary_str;
	}
	else
	{
		larger 	= b->length;
		smaller = a->length;
		l = b->binary_str;
		s = a->binary_str;
	}

	char *copy_buffer = (char *)malloc(sizeof(char) *total);
	char *shift_string = (char *)malloc(sizeof(char) * total);
	memset(shift_string, 0, total);
	memset(copy_buffer, 0, total);


	// copy multiplicant to buffer
	for(int i=0; i < larger; i++)
	{
		shift_string[i] = l[i];
	}

	//shift and multiply

	mpn_z shift, copy;
	shift.length = total;
	

	
	shift.binary_str = shift_string;
	copy.binary_str = copy_buffer;
	copy.length = 1;
	c->binary_str = ctmp;

	//create initial copy of multiplicand 
	//copy_buff(shift.binary_str, copy.binary_str, total);
	
	for(int i=0; i < smaller ; i++)
	{
		if(s[i] == 1)
		{
			mpn_add(&shift, &copy, c);
			copy_buff(c->binary_str, copy.binary_str, total);
			copy.length = c->length;
	  }
		
    shift_right(shift_string, total);
	}

  remove_zeros(c, c->length);
	free(shift_string);
	free(copy_buffer);
}

void remove_zeros(mpn_z *string, unsigned long len)
{
	for(unsigned long i=len-1; i > 0;i--)
	{
		if(string->binary_str[i] == 1)
		{
			string->length = i+1;
			break;
		}
	}

}

void copy_buff(char *src, char *dest, unsigned long sze)
{
	for(unsigned long i=0; i < sze; i++)
	{
		dest[i] = src[i];
	}

}







void mpn_divide(mpn_z *a, mpn_z *b,  mpn_z *c)
{



}





