#ifndef _LIBSCRYPT_H_
#define _LIBSCRYPT_H_
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BCHAR char *
#define BCD 4

//string represented in base 2 lsd first 0⁷ú
typedef struct mpn_t {
	char *binary_str;
	char *base10_str;
	unsigned long 	length;
	unsigned long		base10_length;
} mpn_z;

char *double_dabble(mpn_z *n);
void reverse_double_dabble(mpn_z *);
void print_bit_nums(mpn_z *);

void 	mpn_print_base10(mpn_z *n);

void 	mpn_print_mstols(char *, unsigned long);
static	unsigned long 	reg_to_number(char *p);
static 	void 	test_reg(char *r);
static 	void 	shift_left(char *scratch_end, unsigned long scratch_length);
static void shift_right(char *, unsigned long );
static void copy_buff(char *, char *, unsigned long);
//add subtract divide multiply and mod

//basic math function
void mpn_add(mpn_z *a, mpn_z *b, mpn_z *c); //a+b = c
void mpn_multiply(mpn_z *a, mpn_z *b, mpn_z *c);
void mpn_modulus();
void mpn_subtract(mpn_z *a, mpn_z *b, mpn_z *c);
void mpn_divide(mpn_z	 *a, mpn_z *b, mpn_z *c);

void remove_zeros(mpn_z *string, unsigned long len);
mpn_z *init();
void mpn_free(mpn_z *a);













#endif
